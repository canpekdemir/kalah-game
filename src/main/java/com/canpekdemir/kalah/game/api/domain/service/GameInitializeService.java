package com.canpekdemir.kalah.game.api.domain.service;

import com.canpekdemir.kalah.game.api.domain.entity.Game;
import com.canpekdemir.kalah.game.api.domain.entity.Pit;
import com.canpekdemir.kalah.game.api.domain.entity.Player;
import com.canpekdemir.kalah.game.api.domain.exception.GameNotFoundException;
import com.canpekdemir.kalah.game.api.domain.exception.WrongPitSelectionException;
import com.canpekdemir.kalah.game.api.domain.exception.WrongPlayTurnException;
import com.canpekdemir.kalah.game.api.domain.repository.GameRepository;
import com.canpekdemir.kalah.game.api.domain.repository.PitRepository;
import com.canpekdemir.kalah.game.api.domain.repository.PlayerRepository;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GameInitializeService {

    private final GameRepository gameRepository;
    private final PlayerService playerService;

    public GameInitializeService(GameRepository gameRepository, PlayerService playerService) {
        this.gameRepository = gameRepository;
        this.playerService = playerService;
    }

    public Game initialize(Long firstPlayerId, Long secondPlayerId) {
        Game game = new Game();
        gameRepository.save(game);

        Player firstPlayer = playerService.initializePlayer(firstPlayerId,game);
        Player secondPlayer = playerService.initializePlayer(secondPlayerId,game);
        decidePlayTurn(game, firstPlayer, secondPlayer);

        gameRepository.save(game);
        return game;
    }

    private void decidePlayTurn(Game game, Player firstPlayer, Player secondPlayer) {
        int randomTurnNumber = RandomUtils.nextInt(2);
        if(randomTurnNumber == 0){
            game.setPlayTurn(firstPlayer);
        } else {
            game.setPlayTurn(secondPlayer);
        }
    }


}
