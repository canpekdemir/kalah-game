package com.canpekdemir.kalah.game.api.domain.service;

import com.canpekdemir.kalah.game.api.domain.entity.Game;
import com.canpekdemir.kalah.game.api.domain.entity.Pit;
import com.canpekdemir.kalah.game.api.domain.entity.Player;
import com.canpekdemir.kalah.game.api.domain.repository.PlayerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PlayerService {

    private static final Integer INITIAL_HOUSE_COUNT = 0;

    private final PlayerRepository playerRepository;
    private final PitService pitService;

    public PlayerService(PlayerRepository playerRepository, PitService pitService) {
        this.playerRepository = playerRepository;
        this.pitService = pitService;
    }

    public Player initializePlayer(Long playerId, Game game){
        Player player = playerRepository.findOne(playerId);
        player.setHouse(INITIAL_HOUSE_COUNT);
        player.setGame(game);

        List<Pit> playerPits = pitService.initializePlayerPits(player);
        player.setPits(playerPits);
        playerRepository.save(player);
        return player;
    }

    public boolean checkPlayerHasNoStones(Player player) {
        boolean firstPlayerHasNoStones = true;
        for(Pit pit : player.getPits()){
            if(pit.getStones() != 0){
                firstPlayerHasNoStones = false;
            }
        }
        return firstPlayerHasNoStones;
    }

    public void putAllStonesToHouse(Player player) {
        Integer totalStones = player.getPits().stream().map(Pit::getStones).reduce(0, Integer::sum);
        player.setHouse(player.getHouse() + totalStones);
    }

    public void updateWhenSequenceIsHouse(Player player) {
        Integer house = player.getHouse();
        player.setHouse(house++);
        playerRepository.save(player);
    }

    public Player findPlayer(Long playerId){
        return playerRepository.findOne(playerId);
    }
}
