package com.canpekdemir.kalah.game.api.domain.service;

import com.canpekdemir.kalah.game.api.domain.entity.Game;
import com.canpekdemir.kalah.game.api.domain.entity.Pit;
import com.canpekdemir.kalah.game.api.domain.entity.Player;
import com.canpekdemir.kalah.game.api.domain.exception.GameNotFoundException;
import com.canpekdemir.kalah.game.api.domain.exception.WrongPitSelectionException;
import com.canpekdemir.kalah.game.api.domain.exception.WrongPlayTurnException;
import com.canpekdemir.kalah.game.api.domain.repository.GameRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class KalahGameService {

    private final GameRepository gameRepository;
    private final PitService pitService;
    private final PlayerService playerService;

    public KalahGameService(GameRepository gameRepository, PitService pitService, PlayerService playerService) {
        this.gameRepository = gameRepository;
        this.pitService = pitService;
        this.playerService = playerService;
    }

    public Game retrieveGame(Long gameId) {
        Game game = gameRepository.getOne(gameId);
        if(game == null){
            throw new GameNotFoundException();
        }
        return game;
    }

    public void playForOnePlayer(Long gameId, Long playerId, Long pitId) {
        Game game = retrieveGame(gameId);
        validatePlayerTurn(playerId, game);

        Player currentPlayer = playerService.findPlayer(playerId);
        Player opponentPlayer = findOpponentPlayer(playerId, game);

        List<Pit> currentPlayerPits = currentPlayer.getPits();
        Pit currentPit = findCurrentPit(pitId, currentPlayerPits);

        Integer currentPitSequence = currentPit.getSequence();
        Integer currentPitStones = currentPit.getStones();

        validateStoneCount(currentPitStones);

        currentPit.setStones(1);//first leave one stone to current pit
        currentPitStones--;
        currentPitSequence++;
        pitService.insert(currentPit);

        for (int i = 0; i < currentPitStones; i++) {
            if(isCurrentPitSequenceInHouse(currentPitSequence)){
                updateWhenSequenceIsHouse(game, currentPlayer);

            } else if(isCurrentPitSequenceInOpponentPit(currentPitSequence)){
                updateWhenSequenceIsOpponent(game, opponentPlayer, currentPitSequence);
            } else {
                Pit newPit = pitService.findByPlayerIdAndSequence(playerId,currentPitSequence);
                Integer stones = newPit.getStones();
                if(isLastStonePlaceEmpty(currentPitStones, i, stones)){ //
                    pitService.updateForLastStoneEmpty(opponentPlayer, currentPitSequence, newPit, stones);
                } else {
                    newPit.setStones(++stones);
                }
                pitService.insert(newPit);
                game.setPlayTurn(opponentPlayer);
            }
            currentPitSequence++;
        }
        decideGameFinished(game, currentPlayer, opponentPlayer);

        gameRepository.save(game);
    }

    private void validatePlayerTurn(Long playerId, Game game) {
        if(!game.getPlayTurn().getId().equals(playerId)){
            throw new WrongPlayTurnException();
        }
    }

    private void validateStoneCount(Integer currentPitStones) {
        if(currentPitStones == 0){
            throw new WrongPitSelectionException();
        }
    }

    private boolean isLastStonePlaceEmpty(Integer currentPitStones, int index, Integer stones) {
        return (index == currentPitStones - 1) && stones == 0;
    }

    private void updateWhenSequenceIsOpponent(Game game, Player opponentPlayer, Integer currentPitSequence) {
        pitService.updateWhenSequenceIsOpponent(opponentPlayer,currentPitSequence);
        game.setPlayTurn(opponentPlayer);
    }

    private boolean isCurrentPitSequenceInOpponentPit(Integer currentPitSequence) {
        return currentPitSequence > 6;
    }

    private void updateWhenSequenceIsHouse(Game game, Player currentPlayer) {
        playerService.updateWhenSequenceIsHouse(currentPlayer);
        game.setPlayTurn(currentPlayer);
    }

    private boolean isCurrentPitSequenceInHouse(Integer currentPitSequence) {
        return currentPitSequence == 6;
    }

    private Pit findCurrentPit(Long pitId, List<Pit> currentPlayerPits) {
        return currentPlayerPits.stream().filter(p -> p.getId().equals(pitId)).findFirst().get();
    }

    private Player findOpponentPlayer(Long currentPlayerId, Game game) {
        return game.getPlayers().stream().filter(p -> !p.getId().equals(currentPlayerId)).findFirst().get();
    }

    private void decideGameFinished(Game game, Player currentPlayer, Player opponentPlayer) {
        boolean gameFinished;

        boolean currentPlayerHasNoStones = playerService.checkPlayerHasNoStones(currentPlayer);
        boolean opponentPlayerHasNoStones = playerService.checkPlayerHasNoStones(opponentPlayer);

        if(currentPlayerHasNoStones){
            playerService.putAllStonesToHouse(opponentPlayer);
            gameFinished = true;
        } else if(opponentPlayerHasNoStones){
            playerService.putAllStonesToHouse(currentPlayer);
            gameFinished = true;
        } else {
            gameFinished = false;
        }
        game.setFinished(gameFinished);
    }
}
