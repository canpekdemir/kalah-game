package com.canpekdemir.kalah.game.api.infrastructure.interceptor;

import com.canpekdemir.kalah.game.api.application.exception.KalahGameApiValidationException;
import com.canpekdemir.kalah.game.api.application.model.response.Response;
import com.canpekdemir.kalah.game.api.application.model.response.ResponseStatusType;
import com.canpekdemir.kalah.game.api.domain.exception.GameNotFoundException;
import com.canpekdemir.kalah.game.api.infrastructure.locale.MessageSourceLocalizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.Clock;
import java.util.Locale;

@RestControllerAdvice
public class RestControllerExceptionHandler {

    public static final String FAILURE = "failure";
    private static final Logger logger = LoggerFactory.getLogger(RestControllerExceptionHandler.class);
    private static final String ERROR_MESSAGE_SPLITTER = ";";
    private MessageSource messageSource;
    private MessageSourceLocalizer messageSourceLocalizer;

    public RestControllerExceptionHandler(MessageSource messageSource, MessageSourceLocalizer messageSourceLocalizer) {
        this.messageSource = messageSource;
        this.messageSourceLocalizer = messageSourceLocalizer;
    }

    @ExceptionHandler(KalahGameApiValidationException.class)
    public ResponseEntity<Response> handleValidationException(KalahGameApiValidationException ex) {
        String message = messageSourceLocalizer.getLocaleMessage(ex.getMessage(), ex.getArguments());
        logger.warn("An error occurred. Message: {}", message);
        Response response = createErrorResponse(message);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
    }


    @ExceptionHandler(GameNotFoundException.class)
    public ResponseEntity<Response> handleStockNotFoundException(GameNotFoundException ex) {
        String message = messageSourceLocalizer.getLocaleMessage(ex.getMessage());
        logger.warn("An error occurred. Message: {}", message);
        Response response = createErrorResponse(message);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(response);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Response> handleException(Locale locale, Exception ex) {
        logger.error("An error occurred: {}", ex);
        Response response = createErrorResponse("common.error.system.occurred", locale);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

    private Response createErrorResponse(String message) {
        String[] errorCodeMessage = message.split(ERROR_MESSAGE_SPLITTER);
        return createResponse(errorCodeMessage[0], errorCodeMessage[1]);
    }

    private Response createResponse(String errorCode, String errorMessage) {
        Response response = new Response();
        response.setStatus(ResponseStatusType.FAILURE.getValue());
        response.setErrorCode(errorCode);
        response.setErrorMessage(errorMessage);
        response.setSystemTime(Clock.systemUTC().millis());
        return response;
    }

    private Response createErrorResponse(String exceptionMessage, Locale locale) {
        Response response = new Response();
        response.setLocale(locale.toString());
        String message = messageSource.getMessage(exceptionMessage, null, locale);
        String[] errorCodeMessage = message.split(ERROR_MESSAGE_SPLITTER);
        return createResponse(errorCodeMessage[0], errorCodeMessage[1]);
    }
}
