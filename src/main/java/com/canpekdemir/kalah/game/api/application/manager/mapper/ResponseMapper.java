package com.canpekdemir.kalah.game.api.application.manager.mapper;

import com.canpekdemir.kalah.game.api.application.model.response.Response;
import com.canpekdemir.kalah.game.api.application.model.response.ResponseStatusType;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ResponseMapper {

    public Response mapToSuccessfulResponse(Response response) {
        response.setStatus(ResponseStatusType.SUCCESS.getValue());
        response.setSystemTime(new Date().getTime());
        return response;
    }

    public Response createSuccessfulResponse() {
        Response response = new Response();
        response.setStatus(ResponseStatusType.SUCCESS.getValue());
        response.setSystemTime(new Date().getTime());
        return response;
    }
}
