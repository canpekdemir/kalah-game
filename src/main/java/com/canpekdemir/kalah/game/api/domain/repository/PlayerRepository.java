package com.canpekdemir.kalah.game.api.domain.repository;

import com.canpekdemir.kalah.game.api.domain.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {


}
