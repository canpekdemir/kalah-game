package com.canpekdemir.kalah.game.api.infrastructure.rest;

import com.canpekdemir.kalah.game.api.application.controller.KalahGameController;
import com.canpekdemir.kalah.game.api.application.manager.KalahGameManager;
import com.canpekdemir.kalah.game.api.application.model.request.GameRequest;
import com.canpekdemir.kalah.game.api.application.model.request.PlayRequest;
import com.canpekdemir.kalah.game.api.application.model.response.GameResponse;
import com.canpekdemir.kalah.game.api.application.model.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RestKalahGameController implements KalahGameController {

    private final KalahGameManager kalahGameManager;

    public RestKalahGameController(KalahGameManager kalahGameManager) {
        this.kalahGameManager = kalahGameManager;
    }

    @Override
    @PostMapping(value = "/api/v1/games")
    @ResponseStatus(HttpStatus.CREATED)
    public GameResponse initializeGame(GameRequest gameRequest) {
        return kalahGameManager.initializeGame(gameRequest);
    }

    @Override
    @GetMapping(value = "/api/v1/games")
    @ResponseStatus(HttpStatus.OK)
    public GameResponse retrieveGame(Long gameId) {
        return kalahGameManager.retrieveGame(gameId);
    }

    @Override
    @PostMapping(value = "/api/v1/games/{gameId}/plays")
    public Response play(@PathVariable Long gameId, @Valid @RequestBody PlayRequest playRequest) {
        return kalahGameManager.play(gameId,playRequest);
    }
}
