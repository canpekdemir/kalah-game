package com.canpekdemir.kalah.game.api.application.manager;

import com.canpekdemir.kalah.game.api.application.converter.GameToResponseConverter;
import com.canpekdemir.kalah.game.api.application.manager.mapper.ResponseMapper;
import com.canpekdemir.kalah.game.api.application.model.request.GameRequest;
import com.canpekdemir.kalah.game.api.application.model.request.PlayRequest;
import com.canpekdemir.kalah.game.api.application.model.response.GameResponse;
import com.canpekdemir.kalah.game.api.application.model.response.Response;
import com.canpekdemir.kalah.game.api.domain.entity.Game;
import com.canpekdemir.kalah.game.api.domain.service.GameInitializeService;
import com.canpekdemir.kalah.game.api.domain.service.KalahGameService;
import org.springframework.stereotype.Service;

@Service
public class KalahGameManager {

    private final KalahGameService kalahGameService;
    private final GameInitializeService gameInitializeService;
    private final GameToResponseConverter gameToResponseConverter;
    private final ResponseMapper responseMapper;

    public KalahGameManager(KalahGameService kalahGameService, GameInitializeService gameInitializeService, GameToResponseConverter gameToResponseConverter, ResponseMapper responseMapper) {
        this.kalahGameService = kalahGameService;
        this.gameInitializeService = gameInitializeService;
        this.gameToResponseConverter = gameToResponseConverter;
        this.responseMapper = responseMapper;
    }

    public GameResponse initializeGame(GameRequest gameRequest) {
        Game game = gameInitializeService.initialize(gameRequest.getFirstPlayerId(), gameRequest.getSecondPlayerId());
        return gameToResponseConverter.convert(game);
    }

    public GameResponse retrieveGame(Long gameId) {
        Game game = kalahGameService.retrieveGame(gameId);
        return gameToResponseConverter.convert(game);
    }

    public Response play(Long gameId, PlayRequest playRequest) {
        kalahGameService.playForOnePlayer(gameId, playRequest.getPlayerId(),playRequest.getPitId());
        return responseMapper.createSuccessfulResponse();
    }
}
