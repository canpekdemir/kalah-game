package com.canpekdemir.kalah.game.api.application.model.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class GameRequest extends Request {

    @NotNull
    @Min(1)
    private Long firstPlayerId;

    @NotNull
    @Min(1)
    private Long secondPlayerId;

    public Long getFirstPlayerId() {
        return firstPlayerId;
    }

    public void setFirstPlayerId(Long firstPlayerId) {
        this.firstPlayerId = firstPlayerId;
    }

    public Long getSecondPlayerId() {
        return secondPlayerId;
    }

    public void setSecondPlayerId(Long secondPlayerId) {
        this.secondPlayerId = secondPlayerId;
    }
}
