package com.canpekdemir.kalah.game.api.application.model.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class PlayRequest extends Request {

    @NotNull
    @Min(1)
    private Long playerId;

    @NotNull
    @Min(1)
    private Long pitId;

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public Long getPitId() {
        return pitId;
    }

    public void setPitId(Long pitId) {
        this.pitId = pitId;
    }
}
