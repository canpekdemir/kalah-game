package com.canpekdemir.kalah.game.api.domain.exception;

public class WrongPitSelectionException extends RuntimeException {

    public WrongPitSelectionException() {
        super("game.error.wrongPit");
    }
}