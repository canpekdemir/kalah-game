package com.canpekdemir.kalah.game.api.domain.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "game")
public class Game {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    private List<Player> players;

    @OneToOne
    @JoinColumn(name = "turn_player_id", referencedColumnName = "id")
    private Player playTurn;

    @OneToOne
    @JoinColumn(name = "winner_player_id", referencedColumnName = "id")
    private Player winner;

    @Type(type = "numeric_boolean")
    @Column(name = "finished")
    private boolean finished;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Player getPlayTurn() {
        return playTurn;
    }

    public void setPlayTurn(Player playTurn) {
        this.playTurn = playTurn;
    }

    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
