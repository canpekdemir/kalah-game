package com.canpekdemir.kalah.game.api.domain.repository;


import com.canpekdemir.kalah.game.api.domain.entity.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Long> {


}
