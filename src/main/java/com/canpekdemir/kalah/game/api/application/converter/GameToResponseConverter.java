package com.canpekdemir.kalah.game.api.application.converter;

import com.canpekdemir.kalah.game.api.application.manager.mapper.ResponseMapper;
import com.canpekdemir.kalah.game.api.application.model.dto.GameDto;
import com.canpekdemir.kalah.game.api.application.model.dto.PitDto;
import com.canpekdemir.kalah.game.api.application.model.dto.PlayerDto;
import com.canpekdemir.kalah.game.api.application.model.response.GameResponse;
import com.canpekdemir.kalah.game.api.domain.entity.Game;
import com.canpekdemir.kalah.game.api.domain.entity.Pit;
import com.canpekdemir.kalah.game.api.domain.entity.Player;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Component
public class GameToResponseConverter {

    private final ResponseMapper responseMapper;

    public GameToResponseConverter(ResponseMapper responseMapper) {
        this.responseMapper = responseMapper;
    }

    public GameResponse convert(Game game){
        GameResponse gameResponse = new GameResponse();

        GameDto gameDto = convertGameToDto(game);

        Player firstPlayer = game.getPlayers().get(0);
        gameDto.setFirstPlayer(convertPlayerToDto(firstPlayer));

        Player secondPlayer = game.getPlayers().get(1);
        gameDto.setSecondPlayer(convertPlayerToDto(secondPlayer));

        responseMapper.mapToSuccessfulResponse(gameResponse);
        return gameResponse;
    }

    private GameDto convertGameToDto(Game game) {
        GameDto gameDto = new GameDto();
        gameDto.setId(game.getId());
        gameDto.setWinnerPlayerId(game.getWinner().getId());
        gameDto.setTurnPlayerId(game.getPlayTurn().getId());
        gameDto.setFinished(game.isFinished());
        return gameDto;
    }

    private PlayerDto convertPlayerToDto(Player player){
        PlayerDto playerDto = new PlayerDto();
        playerDto.setId(player.getId());
        playerDto.setHouse(player.getHouse());

        playerDto.setPits(convertPitsToDtoList(player.getPits()));
        return playerDto;
    }

    private List<PitDto> convertPitsToDtoList(List<Pit> pits){
        List<PitDto> pitDtoList = new ArrayList<>();

        Collections.sort(pits, Comparator.comparing(Pit::getSequence));
        for (Pit pit: pits) {
            PitDto pitDto = new PitDto();
            pitDto.setId(pit.getId());
            pitDto.setSequence(pit.getSequence());
            pitDto.setStones(pit.getStones());
            pitDtoList.add(pitDto);
        }
        return pitDtoList;
    }
}
