package com.canpekdemir.kalah.game.api.application.model.response;

import com.canpekdemir.kalah.game.api.application.model.dto.GameDto;

public class GameResponse extends Response {

    private GameDto game;

    public GameDto getGame() {
        return game;
    }

    public void setGame(GameDto game) {
        this.game = game;
    }
}
