package com.canpekdemir.kalah.game.api.application.model.dto;

import java.util.ArrayList;
import java.util.List;

public class PlayerDto {

    private Long id;
    private Integer house;
    private List<PitDto> pits = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }

    public List<PitDto> getPits() {
        return pits;
    }

    public void setPits(List<PitDto> pits) {
        this.pits = pits;
    }
}
