package com.canpekdemir.kalah.game.api.domain.repository;

import com.canpekdemir.kalah.game.api.domain.entity.Pit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PitRepository extends JpaRepository<Pit, Long> {

    Pit findByPlayerIdAndSequence(Long playerId,Integer sequence);
}
