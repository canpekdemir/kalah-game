package com.canpekdemir.kalah.game.api.domain.exception;

public class WrongPlayTurnException extends RuntimeException {

    public WrongPlayTurnException() {
        super("game.error.wrongTurn");
    }
}