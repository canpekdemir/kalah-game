package com.canpekdemir.kalah.game.api.application.model.dto;

public class GameDto {

    private Long id;
    private PlayerDto firstPlayer;
    private PlayerDto secondPlayer;
    private Long winnerPlayerId;
    private Long turnPlayerId;
    private boolean finished;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PlayerDto getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(PlayerDto firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public PlayerDto getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(PlayerDto secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public Long getWinnerPlayerId() {
        return winnerPlayerId;
    }

    public void setWinnerPlayerId(Long winnerPlayerId) {
        this.winnerPlayerId = winnerPlayerId;
    }

    public Long getTurnPlayerId() {
        return turnPlayerId;
    }

    public void setTurnPlayerId(Long turnPlayerId) {
        this.turnPlayerId = turnPlayerId;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
