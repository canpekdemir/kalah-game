package com.canpekdemir.kalah.game.api.application.controller;

import com.canpekdemir.kalah.game.api.application.model.request.GameRequest;
import com.canpekdemir.kalah.game.api.application.model.request.PlayRequest;
import com.canpekdemir.kalah.game.api.application.model.response.GameResponse;
import com.canpekdemir.kalah.game.api.application.model.response.Response;

public interface KalahGameController {

    GameResponse initializeGame(GameRequest gameRequest);

    GameResponse retrieveGame(Long gameId);

    Response play(Long gameId,PlayRequest playRequest);
}
