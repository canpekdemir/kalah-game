package com.canpekdemir.kalah.game.api.domain.exception;

public class GameNotFoundException extends RuntimeException {

    public GameNotFoundException() {
        super("game.error.notFound");
    }
}