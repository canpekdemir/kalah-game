package com.canpekdemir.kalah.game.api.application.exception;

public class KalahGameApiValidationException extends RuntimeException {

    private final Object[] arguments;

    public KalahGameApiValidationException(String message) {
        super(message);
        arguments = new Object[0];
    }

    public KalahGameApiValidationException(String message, Object... arguments) {
        super(message);
        this.arguments = arguments;
    }

    public Object[] getArguments() {
        return arguments;
    }
}