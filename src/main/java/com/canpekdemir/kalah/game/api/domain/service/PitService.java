package com.canpekdemir.kalah.game.api.domain.service;

import com.canpekdemir.kalah.game.api.domain.entity.Game;
import com.canpekdemir.kalah.game.api.domain.entity.Pit;
import com.canpekdemir.kalah.game.api.domain.entity.Player;
import com.canpekdemir.kalah.game.api.domain.repository.PitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PitService {

    private static final int INITIAL_PIT_COUNT = 6;
    private static final Integer INITIAL_STONE_COUNT = 6;

    private final PitRepository pitRepository;

    public PitService(PitRepository pitRepository) {
        this.pitRepository = pitRepository;
    }

    public List<Pit> initializePlayerPits(Player player) {
        List<Pit> playerPits = new ArrayList<>(INITIAL_PIT_COUNT);
        for (int i = 0; i < INITIAL_PIT_COUNT; i++) {
            Pit pit = new Pit();
            pit.setPlayer(player);
            pit.setStones(INITIAL_STONE_COUNT);
            pit.setSequence(i);
            pitRepository.save(pit);
            playerPits.add(pit);
        }
        return playerPits;
    }

    public void updateWhenSequenceIsOpponent(Player opponentPlayer, Integer currentPitSequence) {
        Pit opponentCurrentPit = pitRepository.findByPlayerIdAndSequence(opponentPlayer.getId(),currentPitSequence - 7);
        Integer stones = opponentCurrentPit.getStones();
        opponentCurrentPit.setStones(++stones);
        pitRepository.save(opponentCurrentPit);
    }

    public void updateForLastStoneEmpty(Player opponentPlayer, Integer currentPitSequence, Pit newPit, Integer stones) {
        Pit opponentCurrentPit = pitRepository.findByPlayerIdAndSequence(opponentPlayer.getId(), Math.abs(currentPitSequence - 5));
        Integer opponentCurrentPitStones = opponentCurrentPit.getStones();
        newPit.setStones(++stones + opponentCurrentPitStones);
        pitRepository.save(opponentCurrentPit);
    }

    public void insert(Pit pit){
        pitRepository.save(pit);
    }

    public Pit findByPlayerIdAndSequence(Long playerId, Integer currentPitSequence) {
        return pitRepository.findByPlayerIdAndSequence(playerId, currentPitSequence);
    }
}
