# J1 - kalah game api coding task

This project provides a RESTful API for kalah game

For the backend an embedded H2 Database is used.
 Project data is initialized upon application start-up using the
*data.sql*

## Important notes about project

>  Restful api standards and versioning patterns are applied while designing restful resources.

>  DDD and Hexagonal architecture(ports & adapters) were applied for packaging style and separated the domain and infrastructure layer.

>  Logging and correlationId interceptors(LogExecutionInterceptor & CorrelationIdInterceptor) were implemented to log and trace the request in a whole process.
Every request started and ended logs are written with correlationId.

>  RestControllerExceptionHandler was implemented to catch validation and business runtime exceptions.
The other generic exceptions are also caught to handle spring-generated error responses.

>  Hikari was used to implement connection pool.

>  LocalizationConfiguration added to localize error messages with EN or TR languages.

>  logback used for logging.

## Endpoints

> /api/v1/games - POST : starts a new game with the requested players

> /api/v1/games - GET : retrieves a game details

> /api/v1/games/{gameId}/plays - POST : lets make a new play of a player